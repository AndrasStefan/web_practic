from django import forms
from django.contrib.auth.hashers import make_password

from simpleapp.models import User, Produs


class SignUpForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ('username', 'password')
        widgets = {
            'password': forms.PasswordInput()
        }

    def _post_clean(self):
        raw_password = self.cleaned_data['password']
        self.cleaned_data['password'] = make_password(raw_password)
        return super(SignUpForm, self)._post_clean()


class LoginForm(forms.Form):
    username = forms.CharField(max_length=30)
    password = forms.CharField(max_length=100, widget=forms.PasswordInput)


class ProdusForm(forms.ModelForm):
    class Meta:
        model = Produs
        fields = ('nume', 'descriere', 'producator', 'pret', 'cantitate', 'poza')
