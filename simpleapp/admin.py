from django.contrib import admin

from simpleapp.forms import SignUpForm
from simpleapp.models import User, Produs


# Register your models here.


class UserAdmin(admin.ModelAdmin):
    form = SignUpForm


admin.site.register(User, UserAdmin)
admin.site.register(Produs)
