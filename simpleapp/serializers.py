from rest_framework import serializers

from simpleapp.models import Produs


class EntitySerializer(serializers.ModelSerializer):
    avatar = serializers.SerializerMethodField()

    def get_avatar(self, entity):
        return entity.poza.url

    class Meta:
        model = Produs
        fields = ('nume', 'avatar')
