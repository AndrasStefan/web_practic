from django.contrib.auth.hashers import check_password
from django.db import connection
from django.http import HttpResponse
from django.shortcuts import render, redirect
from rest_framework.response import Response
from rest_framework.views import APIView

from simpleapp.forms import LoginForm, SignUpForm, ProdusForm
from simpleapp.models import User, Produs
from simpleapp.serializers import EntitySerializer


def sql_injection_query(user):
    with connection.cursor() as cursor:
        query = "SELECT * FROM simpleapp_user WHERE username = '%s'" % user
        cursor.execute(query)
        row = cursor.fetchone()
    return row


def index(request):
    pk = request.session.get('user_id')
    if pk:
        return redirect('simpleapp:adauga_produs')

    produse = Produs.objects.all()
    return render(request, 'index.html', {'produse': produse})


def login(request):
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']

            # user = User.objects.filter(username=username).first()
            user = User(*sql_injection_query(username))
            if not username:
                return HttpResponse('User-ul nu exista')

            if check_password(password, user.password):
                request.session['user_id'] = user.pk
                return redirect('simpleapp:index')
            else:
                return HttpResponse('Parola nu e corecta')
    else:
        form = LoginForm()

    return render(request, 'login.html', {'form': form})


def sign_up(request):
    if request.method == "POST":
        form = SignUpForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']

            u = User(username=username, password=password)
            u.save()

            return redirect('simpleapp:index')
    else:
        form = SignUpForm()

    return render(request, 'login.html', {'form': form})


def logout(request):
    try:
        del request.session['user_id']
    except KeyError:
        pass

    return redirect('simpleapp:index')


def create_produs(request):
    if request.method == 'POST':
        form = ProdusForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect('simpleapp:index')
    else:
        form = ProdusForm()
    return render(request, 'adauga_produs.html', {
        'form': form
    })


class EntityList(APIView):
    def get(self, request, pattern):
        produse = Produs.objects.filter(nume__contains=pattern)
        serializer = EntitySerializer(produse, many=True)
        return Response(serializer.data)

