from django.conf.urls.static import static
from django.urls import path

from simpleapp import views
from web_repet import settings

app_name = 'simpleapp'
urlpatterns = [
    path('', views.index, name='index'),
    path('login/', views.login, name='login'),
    path('logout/', views.logout, name='logout'),
    path('signup/', views.sign_up, name='sign_up'),
    path('adauga_produs/', views.create_produs, name="adauga_produs"),
    path('search/<str:pattern>', views.EntityList.as_view(), name="search"),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
