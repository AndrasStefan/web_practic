from django.db import models

# Create your models here.


class User(models.Model):
    username = models.CharField(max_length=30, unique=True)
    password = models.CharField(max_length=100)

    def __str__(self):
        return self.username


class Produs(models.Model):
    nume = models.CharField(max_length=100)
    descriere = models.CharField(max_length=100)
    producator = models.CharField(max_length=100)
    pret = models.IntegerField()
    cantitate = models.IntegerField()
    poza = models.ImageField(upload_to='avatars/%Y/%m/%d/', default='default.jpg')

    def __str__(self):
        return self.nume
